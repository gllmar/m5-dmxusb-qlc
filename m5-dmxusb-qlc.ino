/* m5-dmxusb-qlc
 * Derived from DMXUSB_FastLED.ino
 * require dmxusb fastled m5atom libs
 */


int RGB_MAP[] = {1, 0, 2}; // GRB mapping

#include "DMXUSB.h"
#include <FastLED.h>
#include "M5Atom.h"

// DMXUSB should receive and transmit data at the highest, most reliable speed possible
// Recommended Arduino baud rate: 115200
// Recommended Teensy 3 baud rate: 2000000 (2 Mb/s)
// DMX baud rate: 250000
// MIDI baud rate: 31250
#define DMXUSB_BAUDRATE 250000 // DMX RATE for QLC+

// Number of LEDs on the strip
#define NUM_LEDS 170 // 1 universe of LED

// For LED chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For LED chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806 define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 26
#define INTERNAL_LED_PIN 27
#define CLOCK_PIN 13

// Define the array of LEDs
CRGB led[1]; // Internal LED
CRGB leds[NUM_LEDS]; // external LEDS

// receive a DMX transmission
void showLEDs(int universe, char buffer[512]) {
  for (int index=0; index < 512; index++) { // for each channel, universe starts at 0
    int value = buffer[index]; // DMX value 0 to 255
    int LEDchannel = (universe*510) + index; // Find the LED number; can't fit a 3-channel fixture on the remaining two channels
    if (LEDchannel <= (NUM_LEDS*3)-1) { // If DMX channel (LEDchannel starts at 0) is in range of LEDs (3 channels per LED for RGB)
      int colorChannel = LEDchannel % 3; // Find the color channel of the LED addressed
      int ledID = (LEDchannel - colorChannel) / 3; // Find the FastLED index of the LED addressed
      if (colorChannel == RGB_MAP[0]) leds[ledID].r = value; // If the channel is red, write the red value of the LED
      if (colorChannel == RGB_MAP[1]) leds[ledID].g = value; // If the channel is green, write the red value of the LED
      if (colorChannel == RGB_MAP[2]) leds[ledID].b = value; // If the channel is blue, write the blue value of the LED
    }
  }
  // internal LED 
  led[0].r=buffer[0];
  led[0].g=buffer[1];
  led[0].b=buffer[2];
  FastLED.show(); // Display the frame after processing all channels
}

DMXUSB DMXPC(
  // Stream serial,
  Serial,
  // int baudrate,
  DMXUSB_BAUDRATE,
  // int mode,
  // With mode==0, the library processes 1 universes for a total of 512 DMX channels
  // With mode==1, the library processes two universes for a total of 1024 DMX channels
  0,
  // void (*dmxInCallback)(int universe, unsigned int index, char buffer[512])
  showLEDs
);



void testLed() {
  led[0] = CRGB(10, 0, 10);
  static int value = 10;
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(value, value, value);
    FastLED.show();
    delay(10);

  }
  delay(1000);
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB(0, 0, 0);
  }
  led[0] = CRGB(0, 0, 0);
  FastLED.show();
}


void setup() {
  M5.begin(false, false, false);
  Serial.begin(DMXUSB_BAUDRATE);

 
  FastLED.addLeds<NEOPIXEL, INTERNAL_LED_PIN>(led, 1);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  testLed();

    
}

void loop() {
  DMXPC.listen();
  M5.update();
  if (M5.Btn.wasPressed()) { testLed(); }
}

